'use strict';

var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    async = require('async'),
    fs = require('fs'),
    path = require('path'),
    mkdirp = require('mkdirp'),
    passport = require('passport');

/**
 * Create user
 */
exports.create = function (req, res, next) {
    var newUser = new User(req.body);
    newUser.provider = 'local';
    newUser.save(function (err) {
        if (err) return res.json(400, err);

        req.logIn(newUser, function (err) {
            if (err) return next(err);

            return res.json(req.user.userInfo);
        });
    });
};

/**
 *  Get profile of specified user
 */
exports.show = function (req, res, next) {
    var userId = req.params.id;

    User.findById(userId, function (err, user) {
        if (err) return next(err);
        if (!user) return res.send(404);

        res.send({ profile: user.profile });
    });
};

/**
 * Update user info
 */
exports.update = function (req, res, next) {
    var userId = req.user._id;
    var newName = String(req.body.name);
    var newAvatar = req.body.newAvatar;
    var oldPass = String(req.body.oldPassword);
    var newPass = String(req.body.newPassword);
    async.waterfall([
        function (cb) {
            User.findById(userId, cb);
        },
        function (user, cb) {
            if (newName) {
                user.name = newName;
            }
            if (newPass) {
                if (user.authenticate(oldPass)) {
                    user.password = newPass;
                } else {
                    cb(true, user);
                }
            }
            cb(null, user);
        },
        function (user, cb) {
            if (newAvatar) {
                var dataUrl = newAvatar.resized ? newAvatar.resized.dataURL : newAvatar.dataURL;
                var base64Data = dataUrl.replace(/^data:image\/png;base64,/, "");
                var uploadDir = __dirname + '../../../app/uploads/' + userId;

                mkdirp(uploadDir, function (err) {
                    if (err) {
                        cb(err, null);
                    }
                    fs.writeFile(uploadDir + '/' + newAvatar.file.name, base64Data, 'base64', function (err) {
                        user.avatar = newAvatar.file.name;
                        cb(err, user);
                    });
                });
            }
            cb(null, user);
        },
        function (user, cb) {
            user.save(function (err) {
                cb(err, user);
            });
        }

    ], function (err, user) {
        if (err) {
            res.send(400);
        }
        res.send(200, {
            userId: userId
        });
    });
};


/**
 * Get current user
 */
exports.me = function (req, res) {
    res.json(req.user || null);
};