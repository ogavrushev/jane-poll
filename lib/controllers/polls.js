'use strict';

var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Poll = mongoose.model('Poll');

exports.list = function (req, res) {
    Poll.find({}, 'question user', function (err, polls) {
        res.json(polls);
    });
};

exports.my = function (req, res) {
    if (!req.session.passport.user) {
        res.send(403);
    }

    var userId = req.session.passport.user;

    Poll.find({ user: userId }, 'question user', function (err, polls) {
        res.json(polls);
    });
};

exports.show = function (req, res) {
    var pollId = req.params.id;
    Poll.findById(pollId, "", {lean: true}, function (err, poll) {
        if (poll) {
            var userVoted,
                userChoice,
                totalVotes = 0;
            for (var c in poll.choices) {
                var choice = poll.choices[c];
                for (var v in choice.votes) {
                    var vote = choice.votes[v];
                    totalVotes++;
                    if (vote.ip === req.header('x-forwarded-for') || vote.ip === req.ip) {
                        userVoted = true;
                        userChoice = { _id: choice._id, text: choice.text };
                    }
                }
            }
            poll.userVoted = userVoted;
            poll.userChoice = userChoice;
            poll.totalVotes = totalVotes;
            res.json(poll);
        } else {
            res.json({error: true});
        }
    });
};

exports.remove = function (req, res) {
    if (!req.session.passport.user) {
        res.send(403);
    }

    Poll.findByIdAndRemove(req.params.id, function (err, poll) {
        if (err) {
            res.send(400);
        }
        res.send(200);
    });
};

exports.reset = function (req, res) {
    if (!req.session.passport.user) {
        res.send(403);
    }

    Poll.findById(req.params.id, "", function (err, poll) {
        if (err) {
            res.send(403);
        }

        poll.choices.forEach(function (v) {
            v.votes = [];
        });

        poll.save(function (err) {
            if (err) {
                res.send(500);
            }
            res.send(200);
        });
    });
};

exports.create = function (req, res) {
    var reqBody = req.body,
        choices = reqBody.choices.filter(function (v) {
            return v.text !== "";
        }),
        pollObj = { question: reqBody.question, choices: choices, user: reqBody.user };
    var poll = new Poll(pollObj);

    poll.save(function (err, doc) {
        if (err) {
            res.send(500);
        }
        res.json(doc);
    });
};