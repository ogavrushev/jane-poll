'use strict';
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var voteSchema = new Schema({ ip: 'String' });

var choiceSchema = new Schema({
    text: String,
    votes: [voteSchema]
});

var PollSchema = new Schema({
    question: { type: String, required: true },
    user: {type: String},
    choices: [choiceSchema]
});

module.exports = mongoose.model('Poll', PollSchema);