'use strict';

angular.module('pollApp')
    .factory('Notify', function() {
        toastr.options.closeButton = true;
        return {
            success: function (message) {
                toastr.success(message);
            },
            warn: function(message) {
                toastr.warning(message);
            },
            error: function (message) {
                toastr.error(message);
            }
        };
    });
