'use strict';

angular.module('pollApp')
    .factory('Poll', function($resource) {
        return $resource('/api/polls/:id', {
            id: '@id'
        }, {
            get: {
                method: 'GET',
                params: {}
            },
            my: {
                method: 'GET',
                params: {
                    id: 'my'
                },
                isArray: true
            },
            destroy: {
                method: 'DELETE',
                params: {
                    id: '@id'
                }
            },
            reset: {
                method: 'PUT',
                params: {
                    id: '@id'
                }
            }
        });
    })
    .factory('socket', function($rootScope) {
       var socket = io.connect();
        return {
            on: function(event, callback) {
                socket.on(event, function() {
                    var args = arguments;
                    $rootScope.$apply(function() {
                        callback.apply(socket, args);
                    })
                });
            },
            emit: function(event, data, callback) {
                socket.emit(event, data, function() {
                    var args = arguments;
                    $rootScope.$apply(function() {
                        if(callback) {
                            callback.apply(socket, args);
                        }
                    })
                })
            }
        }
    });
