'use strict';

angular.module('pollApp')
  .factory('Session', function ($resource) {
    return $resource('/api/session/');
  });
