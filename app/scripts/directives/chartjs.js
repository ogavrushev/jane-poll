'use strict';

angular.module('pollApp')
    .directive('chartjs', function () {
        return {
            restrict: "E",
            scope: {
                data: '=data',
                options: '=options',
                width: "=width",
                height: "=height",
                responsive: "@"
            },
            template: '<canvas></canvas>',
            link: function (scope, elt, attrs) {
                var canvas = elt[0].firstChild;
                var ctx = canvas.getContext('2d');

                scope.generate = function () {
                    scope.instance = eval('new Chart(ctx).' + attrs.isType + '(scope.data, scope.options)');
                };

                scope.$watch('data', function () {
                    try {
                        scope.generate();
                    } catch (e) {
                        console.log(e.message);
                    }
                }, true);
            }
        }
    });
