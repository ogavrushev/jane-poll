'use strict';

angular
    .module('pollAppFilters', [])
    .filter('isMy', function($rootScope) {
        return function(input) {
            return input.user && input.user === $rootScope.currentUser.id;
        }
    });