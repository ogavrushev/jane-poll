'use strict';

app.controller('SettingsCtrl', function ($scope, User, Auth, Notify) {
    $scope.errors = {};
    $scope.user = Auth.currentUser();

    $scope.changeAvatar = function () {
        var fileInput = document.querySelector('#file-input');
        fileInput.click();
    };

    $scope.update = function (form) {
        $scope.submitted = true;
        if (form.$valid) {
            Auth.update($scope.user)
                .then(function () {
                    Notify.success('Ваш профиль обновлен');
                })
                .catch(function (err) {
                    Notify.error('Произошла ошибка');
                    console.log(err);
                });
        }
    };
});
