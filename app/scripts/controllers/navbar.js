'use strict';

angular.module('pollApp')
    .controller('NavbarCtrl', function ($scope, $location, Auth) {
        $scope.menu = [
            {
                'title': 'Главная',
                'link': '/'
            },
            {
                'title': 'Опросы',
                'link': '/polls'
            }
        ];

        $scope.logout = function () {
            if (!confirm('Вы действительно хотите выйти?')) {
                return;
            }
            Auth.logout()
                .then(function () {
                    $location.path('/login');
                });
        };

        $scope.isActive = function (route) {
            return route === $location.path();
        };
    });
