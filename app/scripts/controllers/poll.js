'use strict';

app.controller('PollCtrl', function ($scope, $routeParams, $http, $rootScope, $location, Notify) {
    var currentUser = $rootScope.currentUser ? $rootScope.currentUser.id : "";
    $scope.poll = {
        "question": "",
        "user": currentUser,
        "choices": [
            {"text": ""},
            {"text": ""},
            {"text": ""}
        ]
    };
    $scope.addChoice = function () {
        $scope.poll.choices.push({"text": ""});
    };
    $scope.createPoll = function () {
        var poll = $scope.poll;
        if (poll.question.length > 0) {
            var choiceCount = 0;
            for (var i = 0, ln = poll.choices.length; i < ln; i++) {
                var choice = poll.choices[i];
                if (choice.text.length > 0) {
                    choiceCount++
                }
            }
            if (choiceCount > 1) {
                $http
                    .post('/api/polls', poll)
                    .success(function (data) {
                        Notify.success('Опрос успешно создан');
                        $location.path('polls');
                    })
                    .error(function () {
                        Notify.error('Ошибка, нельзя создать опрос');
                    });
            } else {
                Notify.warn('Введите минимум 2 ответа');
            }
        } else {
            Notify.warn('Введите вопрос');
        }
    };
}).controller('PollListCtrl', function ($scope, $http, $rootScope, Poll, Notify, $routeParams) {
    if ($routeParams.my) {
        var polls = Poll.my();
    } else {
        var polls = Poll.query();
    }
    $scope.polls = polls;

    $scope.destroy = function (poll) {
        Poll.destroy({
            id: poll._id
        }).$promise.then(function () {
                $scope.polls.splice($scope.polls.indexOf(poll), 1);
                Notify.success('Опрос перенесен в архив');
            }).catch(function (err) {
                Notify.error('Ошибка, нельзя перенести в архив');
            });

    };

    $scope.reset = function (poll) {
        Poll.reset({
            id: poll._id
        }).$promise.then(function () {
                Notify.success('Результаты опроса сброшены');
            }).catch(function (err) {
                Notify.error('Ошибка, нельзя сбросить результаты опроса');
            });
    };

    $scope.isMyPoll = function (poll) {
        if ($rootScope.currentUser) {
            return poll.user && poll.user === $rootScope.currentUser.id;
        }
        return false;
    };
}).controller('PollItemCtrl', function ($scope, $routeParams, socket, Poll, Notify, Color) {

    function chart(data) {
        var colors = [],
            chartObj = { data: [] },
            choices = data.choices || {};
        for (var choice in choices) {
            colors.push(Color.generate());
            if (choices[choice].votes.length) {
                chartObj.data.push({
                    value: choices[choice].votes.length,
                    color: colors[choice],
                    text: choices[choice].text
                });
            }
        }
        return chartObj;
    }

    $scope.poll = Poll.get({
        id: $routeParams.id
    }, function (response) {
        $scope.MyChart = chart(response);
    });

    socket.on('myvote', function (data) {
        if (data._id === $routeParams.id) {
            $scope.poll = data;
            $scope.MyChart = chart(data);
        }
    });

    socket.on('vote', function (data) {
        if (data._id === $routeParams.id) {
            $scope.poll.choices = data.choices;
            $scope.poll.totalVotes = data.totalVotes;
        }
    });

    $scope.vote = function () {
        var pollId = $scope.poll._id,
            choiceId = $scope.poll.userVote;
        if (choiceId) {
            var voteObj = { poll_id: pollId, choice: choiceId };
            socket.emit('send:vote', voteObj);
            Notify.success('Спасибо за Ваш голос');
        } else {
            Notify.warn('Выберите вариант ответа');
        }
    };
});
